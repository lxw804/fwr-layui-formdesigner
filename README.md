## 项目说明
本项目是一个基于layui表单组件的表单设计器，秉承Layui的设计风格，符合Layui插件规范，不需要进行nodejs编译，直接使用。
1. 本项目基于Layui、Jquery、Sortable
2. 项目中的js代码格式化使用了js_beautify
3、项目已经基本实现了拖动布局，父子布局
4、项目实现了大部分基于Layui的Form表单控件布局，包括输入框、编辑器、下拉、单选、单选组、多选组、日期、滑块、评分、轮播、图片、颜色选择
5、项目实现了一行多列布局

 
## 开发计划
- 开发基于 layui 的表单布局
- 开发基于数据库的表单重新布局

## 开发日志
### 2020-04-18
当天增加计划说明
- 增加数据库导入的数据结构包括了数据库.数据表.数据字段
{
	tableName:abc,
	tableId:1,
	data:[
		{
			columnId:1,
			columnName:"dept_name",
			dataType:"varchar",
			javaType:"String",
			javaField:"deptName",
			pk:true,
			required:true,
			insert:true,
			update:true,
			delete:true,
			list:true,
			query:true,
			sort:1,
			defaultValue:"s",


		}
	]
}
- 根据数据库结构生成表单元素
- 可在线修改数据库结构的表单组件展示状态
- 拖动布局
- 一键布局
- 生成java spring 的后台代码


### 2020-04-08
- 增加关于说明
- 解决拖动到grid中不能默认选择的bug
- 增加导入json功能
- 优化拖动过程中的背景为虚线方框（height:40px）
- 增加对表单的检测，并在input等控件中增加表达式设计如使用 email 或 mobile 控件

### 2020-04-07
- 修复一些不能用的控件属性
- 修改了除了image控件外的所有空间属性
- 使用 [js混淆代码](https://obfuscator.io/)


### 2020-04-06
- 修正只读、禁用、必填选项问题
- 增加获取生成的form表单html可以直接在html中运行


### 2020-04-04
- 更新算法，不在用selectIndex来索引，而是ID
- 根据最新的算法,可以支持无限极嵌套布局（grid嵌套）
- 重新整合导出json数据
- 修复select下拉框宽度改变没有反应
- 修复radio删除问题
- 修复评分控件问题
 
### 2020-04-03
- 重构表单设计区组件名称为 formDesigner.js 使其符合 layui 组件规范
- 重构的组件，把整个html都包括进了组件中，不用再外部提供html支持
- 重构的组件把 css 样式独立出来
- 支持标识的更改 更改后重新显然设计区
- 修正从布局控件中拖动到非布局控件中的时候的bug
- 修复删除布局内的组件就会删除整个布局的问题

### 2020-03-27
- 复选框，当点击checkbox的时候同步显示设计区域
- 增加评分
- 增加轮播图
- 增加颜色选择器
- 增加上传图片
- 增加日期
- 增加开关控件
- 增加数据导出弹框


### 2020-03-26
- 增加grid可以设置多列
- 修正多列大于2列的点击事件不能显示拖动图标的bug
- 增加select下拉控件值，下拉控件的option操作比较复杂
- 增加 radio 单选组
- 增加 checkbox 复选组
- 增加 checkbox 新增的时候同步视图，使用 #ID
- 增加 checkbox 删除的时候同步设计视图，使用 #ID

### 2020-03-25 
- 从组件拖动到设计区，设置 com.selectItem 为当前选择元素
- 从设计区拖动的一级层次组件，设置 com.selectItem 为当前选择元素，并设置 com.selectItem.index=evt.newIndex
- 从组件拖动到设计区的grid子布局，实现子布局控件为当前布局选项
- 重新整理拖动逻辑
 - 从组件去拖动，拖动到表单设计区，拖动到grid设计区
 - 从设计区拖动，拖动到表单设计区，拖动到grid设计区
- copy 问题
- renderForm copy 完成后显示焦点控件问题 没有设置selectItem？ 因为拷贝后未对index的值设置或者renderForm中没对子控件的index设置值
- 为什么控件要点击第二次才能选择到子控件？（因为当设置选中项目的时候 没有设置copy的事件信息，copy的事件信息只在点击的时候设置了。）(解决：在初始化的时候加入)
- 点击子控件，显示的属性不正确 (已解决，因为设置属性的时候没有使用 selectItem 来设置)